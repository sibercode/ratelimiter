package ratelimiter

import (
	"errors"
	"ratelimiter/list"
	"sync"
	"time"
)

var ErrRateLimitExceeded = errors.New("rate limit exceeded")

func NewLimiter(count int, duration time.Duration) *Limiter {
	return &Limiter{
		list:     list.New(count),
		duration: duration,

		mutex: new(sync.Mutex),
	}
}

type Limiter struct {
	list     *list.List
	duration time.Duration

	mutex *sync.Mutex
}

func (l *Limiter) Do(fn func() error) error {
	l.mutex.Lock()
	defer l.mutex.Unlock()

	if node := l.list.FindNodeWithoutTime(); node != nil {
		node.SetTime(time.Now())

		return fn()
	}

	if time.Now().Sub(*l.list.Head.Time()).Nanoseconds() < l.duration.Nanoseconds() {
		return ErrRateLimitExceeded
	}

	l.list.Head.SetTime(time.Now())
	l.list.MoveHeadToTail()

	return fn()
}
