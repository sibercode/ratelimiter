package list

import "time"

func newNode(t *time.Time) *Node {
	return &Node{
		t:    t,
		next: nil,
	}
}

type Node struct {
	t    *time.Time
	next *Node
}

func (n *Node) Next() *Node {
	return n.next
}

func (n *Node) Time() *time.Time {
	return n.t
}

func (n *Node) SetTime(t time.Time) {
	n.t = &t
}

func (n *Node) SetNext(next *Node) {
	n.next = next
}

func New(size int) *List {
	list := &List{
		size: size,
		Head: nil,
		Tail: nil,
	}

	for i := 0; i < size; i++ {
		list.addNode(newNode(nil))
	}

	return list
}

type List struct {
	Head *Node
	Tail *Node
	size int
}

func (l *List) MoveHeadToTail() {
	head := l.Head

	l.Head = head.Next()
	l.Tail.SetNext(head)
}

func (l *List) FindNodeWithoutTime() *Node {
	if l.Tail.t != nil {
		return nil
	}

	for node := l.Head; node != nil; node = node.Next() {
		if node.t != nil {
			continue
		}

		return node
	}

	return nil
}

func (l *List) addNode(node *Node) {
	defer func() {
		node.next = l.Head
	}()

	if l.Head == nil {
		l.Head = node
		l.Tail = node

		return
	}

	l.Tail.next = node
	l.Tail = node
}
